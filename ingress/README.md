# Gaia-X Lab nginx ingress configuration

This folder hosts the helm values file for the lab kubernetes nginx ingress deployment.


## Usage

```shell
helm upgrade --install -n ingress-nginx ingress-nginx ingress-nginx/ingress-nginx -f values.yaml
```

## Specific aspects

We enabled TCP routing on the nginx, the TCP ports used are at the bottom of the values file in `tcp` array

We also rely on snippet annotation to enable URL rewrite to maintain TAGUS shape availability. The option `allowSnippetAnnotations` is set to true
The ingress that produces this rewriting has to be pushed to the cluster after nginx deployment

```shell
kubectl apply -f ingress-redirect.yaml
```
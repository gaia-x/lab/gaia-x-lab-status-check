# Gaia-X Lab deployment charts

This project contains charts to deploy and maintain instances of service we need in the Gaia-X Lab

## Status pages
An instance of [Uptime Kuma](https://github.com/louislam/uptime-kuma).

The purpose is to check whether all services provided by Gaia-X Lab are up and running and provide a status page

- [Clearing Houses](https://status.lab.gaia-x.eu/status/gxdch)
- [Lab deployments](https://status.lab.gaia-x.eu/status/v1)